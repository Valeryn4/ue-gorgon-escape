// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "AbilitySystemComponent.h"
#include "Engine/HitResult.h"
#include "CoreInterface.generated.h"



// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCoreInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 *
 * 
 */
class GORGONESCAPE_API ICoreInterface
{
	GENERATED_BODY()


public:

    /** ASC */
    /**
    * @return AbilitySystemComponent
    */
    UFUNCTION(Category = "Core|ASC")
    virtual class UAbilitySystemComponent* GetASC() const = 0;

    /** Attributes */
    /*
    * @return CoreAttributeSet pointer
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual class UCoreAttributeSet* GetCoreAttributeSet() const = 0;

    /*
    * @param TagContainer reference FGameplayTagContainer
    */
    UFUNCTION(Category = "Core|GameplayTags")
    virtual void GetGameplayTags(FGameplayTagContainer& TagContainer) const = 0;
    /*
    * @return HealthCurrent from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetHealthCurrent() const = 0;
    /*
    * @return HealthMax from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetHealthMax() const = 0;
    /*
    * @return HealthRegeneration from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetHealthRegeneration() const = 0;

    /*
    * @return StaminaCurrent from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetStaminaCurrent() const = 0;
    /*
    * @return StaminaMax from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetStaminaMax() const = 0;
    /*
    * @return StaminaRegeneration from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetStaminaRegeneration() const = 0;

    /*
    * @return SpeedMovement from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetSpeedMovement() const = 0;

    /*
    * @return AttackSpeed from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetAttackSpeed() const = 0;
    /*
    * @return AttackDamage from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetAttackDamage() const = 0;
    /*
    * @return AttackImpact from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetAttackImpact() const = 0;

    /*
    * @return AttackProjectileSpeed from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetAttackProjectileSpeed() const = 0;
    /*
    * @return AttackProjectileRange from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetAttackProjectileRange() const = 0;

    /*
    * @return Strength from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetStrength() const = 0;
    /*
    * @return Weight from attribute sets
    */
    UFUNCTION(Category = "Core|Attribute")
    virtual float GetWeight() const = 0;

    /** Callback attributes*/
    /*
    * @param Source source actor damage emitter
    * @param DamageValue applied damage health
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Core|Attribute")
    void OnDamageHealth(AActor *Source, const float DamageValue, const FHitResult &Hit);
    virtual void OnDamageHealth_Implementation(AActor* Source, const float DamageValue, const FHitResult& Hit);

    /*
    * @param Source source actor damage emitter
    * @param DamageValue applied damage stamina
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Core|Attribute")
    void OnDamageStamina(AActor* Source, const float DamageValue);
    virtual void OnDamageStamina_Implementation(AActor* Source, const float DamageValue);

    /*
    * @param Source source actor healing emitter
    * @param DamageValue applied healing health
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Core|Attribute")
    void OnHealingHealth(AActor* Source, const float DamageValue);
    virtual void OnHealingHealth_Implementation(AActor* Source, const float DamageValue);

    /*
    * @param Source source actor healing emitter
    * @param DamageValue applied healing stamina
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Core|Attribute")
    void OnHealingStamina(AActor* Source, const float DamageValue);
    virtual void OnHealingStamina_Implementation(AActor* Source, const float DamageValue);

    /***/
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Core|Attribute")
    void OnUpdateAttribute();
    virtual void OnUpdateAttribute_Implementation();

    /** Abilities */
    UFUNCTION(Category = "Core|Ability")
    virtual FGameplayAbilitySpecHandle AddAbility(TSubclassOf<class UGameplayAbility> AbilityClass) = 0;

    UFUNCTION(Category = "Core|Ability")
    virtual void RemoveAbility(const FGameplayAbilitySpecHandle& Ability) = 0;

    UFUNCTION(Category = "Core|Ability")
    virtual void GetAbilityList(TArray<FGameplayAbilitySpecHandle>& OutAbilityHandles) const = 0;

    UFUNCTION(Category = "Core|Ability")
    virtual void ExecuteAbility(const FGameplayAbilitySpecHandle& Handler) = 0;

    /** Information */
    UFUNCTION(Category = "Core|State")
    virtual bool GetIsAlive() const = 0;

    /**
    * Initialize
    */


    UFUNCTION(Category = "Core|Initialize")
    virtual void InitializeAbilities() = 0;

    UFUNCTION(Category = "Core|Initialize")
    virtual void InitializeAttributes() = 0;

    UFUNCTION(Category = "Core|Initialize")
    virtual void InitializeStartingEffects() = 0;
    
    /**
    * 
    */

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Core|API")
    void OnDead();
    virtual void OnDead_Implementation();

    UFUNCTION(Category = "Core|API")
    virtual void Dead() = 0;


};
