// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "CoreAttributeSet.generated.h"


// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)


/**
 *
 * Base Universal character humanoid attribute
 */
UCLASS()
class GORGONESCAPE_API UCoreAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:

    /** Health collection */
    /**
    * @param HealthCurrent range (0.0 - @param HealthMax)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
    FGameplayAttributeData HealthCurrent;

    /**
    * @param HealthMax any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
    FGameplayAttributeData HealthMax;

    /**
    * @param HealthRegeneration any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
    FGameplayAttributeData HealthRegeneration;

    /** Stamina collection */

    /**
    * @param StaminaCurrent range (0.0 - @param StaminaMax)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
    FGameplayAttributeData StaminaCurrent;

    /**
    * @param StaminaMax any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
    FGameplayAttributeData StaminaMax;

    /**
    * @param StaminaRegeneration any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
    FGameplayAttributeData StaminaRegeneration;


    /**
    * @param SpeedMovement range (1.0 - 0.0)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    FGameplayAttributeData SpeedMovement;

    /**
    * @param AbilitySpeedReload range (1.0 - 0.0)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
    FGameplayAttributeData AbilitySpeedReload;

    /**
    * @param AbilitySpeedActivate range (1.0 - 0.0)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
    FGameplayAttributeData AbilitySpeedActivate;


    /**
    * @param AttackSpeed range (1.0 - 0.0)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
    FGameplayAttributeData AttackSpeed;

    /**
    * @param AttackDamage any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
    FGameplayAttributeData AttackDamage;

    /**
    * @param AttackImpact any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
    FGameplayAttributeData AttackImpact;

    /**
    * @param AttackProjectileSpeed range (1.0 - 0.0)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
    FGameplayAttributeData AttackProjectileSpeed;

    /**
    * @param AttackProjectileRange range (1.0 - 0.0)
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
    FGameplayAttributeData AttackProjectileRange;


    /**
    * @param Strength any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Base")
    FGameplayAttributeData Strength;

    /**
    * @param Weight any value
    */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Base")
    FGameplayAttributeData Weight;


    /** Meta attributes*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta")
    FGameplayAttributeData DamageHealth;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta")
    FGameplayAttributeData DamageStamina;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta")
    FGameplayAttributeData HealingHealth;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meta")
    FGameplayAttributeData HealingStamina;

    //Accessors 
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, HealthCurrent);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, HealthMax);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, HealthRegeneration);
    
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, StaminaCurrent);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, StaminaMax);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, StaminaRegeneration);

    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, SpeedMovement);

    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, AbilitySpeedReload);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, AbilitySpeedActivate);

    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, AttackSpeed);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, AttackDamage);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, AttackImpact);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, AttackProjectileSpeed);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, AttackProjectileRange);

    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, Strength);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, Weight);

    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, DamageHealth);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, DamageStamina);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, HealingHealth);
    ATTRIBUTE_ACCESSORS(UCoreAttributeSet, HealingStamina);


    /**
    * @param Attribute - Attribute do change
    * @param NewValue - ref value do change
    */
    virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
    virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;

    /**
    * @Data data apply effects
    */
    virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;


    /**
    * UTILS
    */
    void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
        const FGameplayAttributeData& MaxAttribute,
        float NewMaxValue,
        const FGameplayAttribute& AffectedAttributeProperty);


};
