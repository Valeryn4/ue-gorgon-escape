// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbilitySystemInterface.h"
#include "Abilities/GameplayAbility.h"
#include "Core/CoreInterface.h"

#include "CoreActor.generated.h"

UCLASS()
class GORGONESCAPE_API ACoreActor : public AActor, public IAbilitySystemInterface, public ICoreInterface
{
	GENERATED_BODY()

protected:
    class UAbilitySystemComponent* ASC = nullptr;
    class UCoreAttributeSet* AttributeSet = nullptr;
public:	
	// Sets default values for this actor's properties
	ACoreActor();
    bool bDead = false;


    // Default abilities for this Character. These will be removed on Character death and regiven if Character respawns.
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Core|Abilities")
    TArray<TSubclassOf<class UGameplayAbility>> InitialAbilities;

    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Core|Abilities")
    TSubclassOf<class UGameplayEffect> InitialEffect;

    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Core|Abilities")
    TArray<TSubclassOf<class UGameplayEffect>> StartingEffects;

    UPROPERTY(BlueprintReadOnly, Category = "Core|Abilities")
    TArray<struct FGameplayAbilitySpecHandle> AbilityList;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    /**
    * Implementation GAS Interface
    */
public:
    virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

    /**
    * Implementation Core Interface
    */
public:



    virtual void InitializeAbilities() override;

    virtual void InitializeAttributes() override;

    virtual void InitializeStartingEffects() override;


    /**
    * ASC
    * @return AbilitySystemComponent
    */
    UFUNCTION(BlueprintCallable, Category = "Core|ASC")
    virtual class UAbilitySystemComponent* GetASC() const override;

    /*
    * @return CoreAttributeSet pointer
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual class UCoreAttributeSet* GetCoreAttributeSet() const  override;


    /*
    * @param TagContainer reference FGameplayTagContainer
    */
    UFUNCTION(Category = "Core|GameplayTags")
    virtual void GetGameplayTags(FGameplayTagContainer& TagContainer) const override;

    /*
    * @return HealthCurrent from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetHealthCurrent() const override;
    /*
    * @return HealthMax from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetHealthMax() const override;
    /*
    * @return HealthRegeneration from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetHealthRegeneration() const override;

    /*
    * @return StaminaCurrent from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetStaminaCurrent() const override;
    /*
    * @return StaminaMax from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetStaminaMax() const override;
    /*
    * @return StaminaRegeneration from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetStaminaRegeneration() const override;

    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetSpeedMovement() const override;

    /*
    * @return AttackSpeed from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetAttackSpeed() const override;
    /*
    * @return AttackDamage from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetAttackDamage() const override;
    /*
    * @return AttackImpact from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetAttackImpact() const override;

    /*
    * @return AttackProjectileSpeed from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetAttackProjectileSpeed() const override;
    /*
    * @return AttackProjectileRange from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetAttackProjectileRange() const override;

    /*
    * @return Strength from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetStrength() const override;
    /*
    * @return Weight from attribute sets
    */
    UFUNCTION(BlueprintCallable, Category = "Core|Attribute")
    virtual float GetWeight() const override;


    /** Abilities */
    UFUNCTION(BlueprintCallable, Category = "Core|Ability")
    virtual FGameplayAbilitySpecHandle AddAbility(TSubclassOf<class UGameplayAbility> AbilityClass) override;

    UFUNCTION(BlueprintCallable, Category = "Core|Ability")
    virtual void RemoveAbility(const FGameplayAbilitySpecHandle& Ability) override;

    UFUNCTION(Category = "Core|Ability")
    virtual void GetAbilityList(TArray<FGameplayAbilitySpecHandle>& OutAbilityHandles) const override;

    UFUNCTION(BlueprintCallable, Category = "Core|Ability")
    virtual void ExecuteAbility(const FGameplayAbilitySpecHandle& Handler) override;


    /** Information */
    UFUNCTION(BlueprintCallable, Category = "Core|State")
    virtual bool GetIsAlive() const override;


    UFUNCTION(Category = "Core|API")
    virtual void Dead() override;

};
