// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Character/CoreAttributeSet.h"
#include "Core/CoreInterface.h"

#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"


void UCoreAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
    Super::PreAttributeChange(Attribute, NewValue);

    if (Attribute == GetHealthMaxAttribute())
    {
        AdjustAttributeForMaxChange(HealthCurrent, HealthMax, NewValue, GetHealthCurrentAttribute());
    }
    else if (Attribute == GetStaminaMaxAttribute())
    {
        AdjustAttributeForMaxChange(StaminaCurrent, StaminaMax, NewValue, GetStaminaCurrentAttribute());
    }
    else if (Attribute == GetHealthCurrentAttribute())
    {
        NewValue = FMath::Clamp(NewValue, 0.f, GetHealthMax());
    }
    else if (Attribute == GetStaminaCurrentAttribute())
    {
        NewValue = FMath::Clamp(NewValue, 0.f, GetStaminaMax());
    }


    
}

void UCoreAttributeSet::PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue)
{

    Super::PostAttributeChange(Attribute, OldValue, NewValue);

    bool bIsMetaAttribute = (
        Attribute == GetHealingHealthAttribute() or
        Attribute == GetHealingStaminaAttribute() or
        Attribute == GetDamageHealthAttribute() or
        Attribute == GetDamageStaminaAttribute()
        );

    if (bIsMetaAttribute) {
        return;
    }

    if (!FMath::IsNearlyEqual(OldValue, NewValue, 0.001f)) {

        UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
        AbilityComp->AbilityActorInfo->AvatarActor.Get();
        AActor* OwnerActor = nullptr;
        ICoreInterface* OwnerCoreActor = nullptr;

        if (AbilityComp->AbilityActorInfo.IsValid() && AbilityComp->AbilityActorInfo->AvatarActor.IsValid())
        {
            OwnerActor = AbilityComp->AbilityActorInfo->AvatarActor.Get();
            OwnerCoreActor = Cast<ICoreInterface>(OwnerActor);
        }
        
        if (OwnerCoreActor)
        {
            ICoreInterface::Execute_OnUpdateAttribute(OwnerActor);
            if (Attribute == GetHealthCurrentAttribute())
            {
                if (NewValue <= 0.f)
                {
                    OwnerCoreActor->Dead();
                    ICoreInterface::Execute_OnDead(OwnerActor);
                }
            }
        }
    }
}

void UCoreAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
    Super::PostGameplayEffectExecute(Data);

    FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
    UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();

    // Get the Target actor, which should be our owner
    AActor* TargetActor = nullptr;
    AController* TargetController = nullptr;
    ICoreInterface* TargetCoreActor = nullptr;
    
    if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
    {
        TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
        TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
        TargetCoreActor = Cast<ICoreInterface>(TargetActor);
    }


    // Get the Source actor
    AActor* SourceActor = nullptr;
    AController* SourceController = nullptr;
    ICoreInterface* SourceCoreActor = nullptr;
    if (Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
    {
        SourceActor = Source->AbilityActorInfo->AvatarActor.Get();
        SourceController = Source->AbilityActorInfo->PlayerController.Get();
        if (SourceController == nullptr && SourceActor != nullptr)
        {
            if (APawn* Pawn = Cast<APawn>(SourceActor))
            {
                SourceController = Pawn->GetController();
            }
        }

        // Use the controller to find the source pawn
        if (SourceController)
        {
            SourceCoreActor = Cast<ICoreInterface>(SourceController->GetPawn());
        }
        else
        {
            SourceCoreActor = Cast<ICoreInterface>(SourceActor);
        }

        // Set the causer actor based on context if it's set
        if (Context.GetEffectCauser())
        {
            SourceActor = Context.GetEffectCauser();
        }
    }



    if (Data.EvaluatedData.Attribute == GetDamageHealthAttribute())
    {
        const float LocalDamageDone = GetDamageHealth();
        SetDamageHealth(0.f);

        // Try to extract a hit result
        FHitResult HitResult;
        if (Context.GetHitResult())
        {
            HitResult = *Context.GetHitResult();
        }

        if (LocalDamageDone > 0.f)
        {
            bool WasAlive = true;
            if (TargetCoreActor)
            {
                WasAlive = TargetCoreActor->GetIsAlive();
            }

            if (WasAlive)
            {
                // Apply the health change and then clamp it
                const float NewHealth = GetHealthCurrent() - LocalDamageDone;
                SetHealthCurrent(FMath::Clamp(NewHealth, 0.0f, GetHealthMax()));

                if (TargetCoreActor)
                {
                    ICoreInterface::Execute_OnDamageHealth(TargetActor, SourceActor, LocalDamageDone, HitResult);
                }
            }
        }

    }
    else if (Data.EvaluatedData.Attribute == GetDamageStaminaAttribute())
    {
        const float LocalDamageDone = GetDamageStamina();
        SetDamageStamina(0.f);

        if (LocalDamageDone > 0.f)
        {
            bool WasAlive = true;
            if (TargetCoreActor)
            {
                WasAlive = TargetCoreActor->GetIsAlive();
            }

            if (WasAlive)
            {
                // Apply the health change and then clamp it
                const float NewStamina = GetStaminaCurrent() - LocalDamageDone;
                SetStaminaCurrent(FMath::Clamp(NewStamina, 0.0f, GetStaminaMax()));

                if (TargetCoreActor)
                {
                    ICoreInterface::Execute_OnDamageStamina(TargetActor, SourceActor, LocalDamageDone);
                }
            }
        }
    }
    else if (Data.EvaluatedData.Attribute == GetHealingHealthAttribute())
    {
        const float LocalHealingDone = GetHealingHealth();
        SetHealingHealth(0.f);

        if (LocalHealingDone > 0.f)
        {
            bool WasAlive = true;
            if (TargetCoreActor)
            {
                WasAlive = TargetCoreActor->GetIsAlive();
            }

            if (WasAlive)
            {
                // Apply the health change and then clamp it
                const float NewValue = GetHealthCurrent() + LocalHealingDone;
                SetHealthCurrent(FMath::Clamp(NewValue, 0.0f, GetHealthMax()));

                if (TargetCoreActor)
                {
                    ICoreInterface::Execute_OnHealingHealth(TargetActor, SourceActor, LocalHealingDone);
                }
            }
        }
    }
    else if (Data.EvaluatedData.Attribute == GetHealingStaminaAttribute())
    {
        const float LocalHealingDone = GetHealingStamina();
        SetHealingStamina(0.f);

        if (LocalHealingDone > 0.f)
        {
            bool WasAlive = true;
            if (TargetCoreActor)
            {
                WasAlive = TargetCoreActor->GetIsAlive();
            }

            if (WasAlive)
            {
                // Apply the health change and then clamp it
                const float NewValue = GetStaminaCurrent() + LocalHealingDone;
                SetStaminaCurrent(FMath::Clamp(NewValue, 0.0f, GetStaminaMax()));

                if (TargetCoreActor)
                {
                    ICoreInterface::Execute_OnHealingStamina(TargetActor, SourceActor, LocalHealingDone);
                }
            }
        }
    }

}

void UCoreAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
    UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
    const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
    if (!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComp)
    {
        // Change current value to maintain the current Val / Max percent
        const float CurrentValue = AffectedAttribute.GetCurrentValue();
        float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

        AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
    }
}



