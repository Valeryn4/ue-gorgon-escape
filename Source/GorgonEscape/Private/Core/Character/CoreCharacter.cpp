// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Character/CoreCharacter.h"
#include "Core/Character/CoreAttributeSet.h"
#include "Core/Character/CoreAttributeSet.h"
#include "AbilitySystemComponent.h"


// Sets default values
ACoreCharacter::ACoreCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    ASC = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("ASC"));
    AttributeSet = CreateDefaultSubobject<UCoreAttributeSet>(TEXT("AttributeSet"));

}

// Called when the game starts or when spawned
void ACoreCharacter::BeginPlay()
{
	Super::BeginPlay();

    InitializeAttributes();
    InitializeAbilities();
    InitializeStartingEffects();
	
}

// Called every frame
void ACoreCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACoreCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

/**
* AbilitySystemInterface
*/

UAbilitySystemComponent* ACoreCharacter::GetAbilitySystemComponent() const
{
    return ASC;
}

void ACoreCharacter::InitializeAbilities()
{
    if (!IsValid(ASC))
    {
        return;
    }

    for (TSubclassOf<UGameplayAbility>& StartupAbility : InitialAbilities)
    {

        ASC->GiveAbility(FGameplayAbilitySpec(StartupAbility, 1, -1, this));
    }

}

void ACoreCharacter::InitializeAttributes()
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    if (!InitialEffect)
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing InitialEffect for %s. Please fill in the character's Blueprint."), *FString(__FUNCTION__), *GetName());
        return;
    }

    // Can run on Server and Client
    FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
    EffectContext.AddSourceObject(this);

    ASC->ApplyGameplayEffectToSelf(InitialEffect->GetDefaultObject<UGameplayEffect>(), 0.f, EffectContext);

}

void ACoreCharacter::InitializeStartingEffects()
{

    if (!IsValid(ASC))
    {
        return;
    }

    FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
    EffectContext.AddSourceObject(this);

    for (TSubclassOf<UGameplayEffect>& Effect : StartingEffects)
    {

        ASC->ApplyGameplayEffectToSelf(Effect->GetDefaultObject<UGameplayEffect>(), 0.f, EffectContext);
    }
}

UAbilitySystemComponent* ACoreCharacter::GetASC() const
{
    return ASC;
}


UCoreAttributeSet* ACoreCharacter::GetCoreAttributeSet() const
{
    return AttributeSet;
}

void ACoreCharacter::GetGameplayTags(FGameplayTagContainer& TagContainer) const
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->GetOwnedGameplayTags(TagContainer);
}

float ACoreCharacter::GetHealthCurrent() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }
    return AttributeSet->GetHealthCurrent();
}

float ACoreCharacter::GetHealthMax() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetHealthMax();
}

float ACoreCharacter::GetHealthRegeneration() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetHealthRegeneration();
}

float ACoreCharacter::GetStaminaCurrent() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStaminaCurrent();
}

float ACoreCharacter::GetStaminaMax() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStaminaMax();
}

float ACoreCharacter::GetStaminaRegeneration() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStaminaRegeneration();
}

float ACoreCharacter::GetSpeedMovement() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetSpeedMovement();
}

float ACoreCharacter::GetAttackSpeed() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackSpeed();
}

float ACoreCharacter::GetAttackDamage() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackDamage();
}

float ACoreCharacter::GetAttackImpact() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackImpact();
}

float ACoreCharacter::GetAttackProjectileSpeed() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackProjectileSpeed();
}

float ACoreCharacter::GetAttackProjectileRange() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackProjectileRange();
}

float ACoreCharacter::GetStrength() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStrength();
}

float ACoreCharacter::GetWeight() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetWeight();
}

FGameplayAbilitySpecHandle ACoreCharacter::AddAbility(TSubclassOf<class UGameplayAbility> AbilityClass)
{
    return ASC->GiveAbility(FGameplayAbilitySpec(AbilityClass, 1, -1, this));
}

void ACoreCharacter::RemoveAbility(const FGameplayAbilitySpecHandle& Ability)
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->ClearAbility(Ability);
}

void ACoreCharacter::GetAbilityList(TArray<FGameplayAbilitySpecHandle>& OutAbilityHandles) const
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->GetAllAbilities(OutAbilityHandles);
    return;
}

void ACoreCharacter::ExecuteAbility(const FGameplayAbilitySpecHandle& Handler)
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->TryActivateAbility(Handler);
}



bool ACoreCharacter::GetIsAlive() const
{
    return !bDead;
}

void ACoreCharacter::Dead()
{
    bDead = true;
}


