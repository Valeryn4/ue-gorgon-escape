// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/CoreInterface.h"
#include "Abilities/GameplayAbility.h"
#include "Core/Character/CoreAttributeSet.h"
#include "AbilitySystemComponent.h"


void ICoreInterface::OnDamageHealth_Implementation(AActor* Source, const float DamageValue, const FHitResult& Hit)
{
}

void ICoreInterface::OnDamageStamina_Implementation(AActor* Source, const float DamageValue)
{
}

void ICoreInterface::OnHealingHealth_Implementation(AActor* Source, const float HealingValue)
{
}

void ICoreInterface::OnHealingStamina_Implementation(AActor* Source, const float HealingValue)
{
}

void ICoreInterface::OnUpdateAttribute_Implementation()
{
}

void ICoreInterface::OnDead_Implementation()
{
}
