// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Actor/CoreActor.h"

#include "Core/Character/CoreCharacter.h"
#include "Core/Character/CoreAttributeSet.h"
#include "Core/Character/CoreAttributeSet.h"


// Sets default values
ACoreActor::ACoreActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    ASC = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("CAS"));
    AttributeSet = CreateDefaultSubobject<UCoreAttributeSet>(TEXT("AttributeSet"));

}

// Called when the game starts or when spawned
void ACoreActor::BeginPlay()
{
	Super::BeginPlay();

    InitializeAttributes();
    InitializeAbilities();
    InitializeStartingEffects();
	
}

// Called every frame
void ACoreActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


/**
* AbilitySystemInterface
*/

UAbilitySystemComponent* ACoreActor::GetAbilitySystemComponent() const
{
    return ASC;
}

void ACoreActor::InitializeAbilities()
{
    if (!IsValid(ASC))
    {
        return;
    }

    for (TSubclassOf<UGameplayAbility>& StartupAbility : InitialAbilities)
    {

        ASC->GiveAbility(FGameplayAbilitySpec(StartupAbility, 1, -1, this));
    }

}

void ACoreActor::InitializeAttributes()
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    if (!InitialEffect)
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing InitialEffect for %s. Please fill in the character's Blueprint."), *FString(__FUNCTION__), *GetName());
        return;
    }

    // Can run on Server and Client
    FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
    EffectContext.AddSourceObject(this);

    ASC->ApplyGameplayEffectToSelf(InitialEffect->GetDefaultObject<UGameplayEffect>(), 0.f, EffectContext);

}

void ACoreActor::InitializeStartingEffects()
{

    if (!IsValid(ASC))
    {
        return;
    }

    FGameplayEffectContextHandle EffectContext = ASC->MakeEffectContext();
    EffectContext.AddSourceObject(this);

    for (TSubclassOf<UGameplayEffect>& Effect : StartingEffects)
    {

        ASC->ApplyGameplayEffectToSelf(Effect->GetDefaultObject<UGameplayEffect>(), 0.f, EffectContext);
    }
}

UAbilitySystemComponent* ACoreActor::GetASC() const
{
    return ASC;
}


UCoreAttributeSet* ACoreActor::GetCoreAttributeSet() const
{
    return AttributeSet;
}

void ACoreActor::GetGameplayTags(FGameplayTagContainer& TagContainer) const
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->GetOwnedGameplayTags(TagContainer);
}

float ACoreActor::GetHealthCurrent() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }
    return AttributeSet->GetHealthCurrent();
}

float ACoreActor::GetHealthMax() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetHealthMax();
}

float ACoreActor::GetHealthRegeneration() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetHealthRegeneration();
}

float ACoreActor::GetStaminaCurrent() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStaminaCurrent();
}

float ACoreActor::GetStaminaMax() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStaminaMax();
}

float ACoreActor::GetStaminaRegeneration() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStaminaRegeneration();
}

float ACoreActor::GetSpeedMovement() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetSpeedMovement();
}

float ACoreActor::GetAttackSpeed() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackSpeed();
}

float ACoreActor::GetAttackDamage() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackDamage();
}

float ACoreActor::GetAttackImpact() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackImpact();
}

float ACoreActor::GetAttackProjectileSpeed() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackProjectileSpeed();
}

float ACoreActor::GetAttackProjectileRange() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetAttackProjectileRange();
}

float ACoreActor::GetStrength() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetStrength();
}

float ACoreActor::GetWeight() const
{
    if (!IsValid(AttributeSet))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init AttributeSets."), *FString(__FUNCTION__), *GetName());
        return -1.0;
    }

    return AttributeSet->GetWeight();
}

FGameplayAbilitySpecHandle ACoreActor::AddAbility(TSubclassOf<class UGameplayAbility> AbilityClass)
{
    return ASC->GiveAbility(FGameplayAbilitySpec(AbilityClass, 1, -1, this));
}

void ACoreActor::RemoveAbility(const FGameplayAbilitySpecHandle& Ability)
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->ClearAbility(Ability);
}

void ACoreActor::GetAbilityList(TArray<FGameplayAbilitySpecHandle>& OutAbilityHandles) const
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->GetAllAbilities(OutAbilityHandles);
    return;
}

void ACoreActor::ExecuteAbility(const FGameplayAbilitySpecHandle& Handler)
{
    if (!IsValid(ASC))
    {
        UE_LOG(LogTemp, Error, TEXT("%s() Missing ASC for %s. Please init Ability System Component."), *FString(__FUNCTION__), *GetName());
        return;
    }

    ASC->TryActivateAbility(Handler);
}



bool ACoreActor::GetIsAlive() const
{
    return !bDead;
}

void ACoreActor::Dead()
{
    bDead = true;
}
